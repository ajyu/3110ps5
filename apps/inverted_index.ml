(* Horace Chan (hhc39)
 * Ansha Yu (ay226)
 * PS5
 *)

module M = Map.Make (struct type t = string let compare = compare end)
module S = Sequence

(* inverted_index computes an inverted index for the contents of
 * a given file. The filename is the given string.
 * The results are output to stdout. *)
let mkindex (args : string ) : unit = 
  let doca = Array.of_list (Util.load_documents args) in
  let docs = S.tabulate (fun x -> doca.(x)) (Array.length doca) in
  let mapdoc d =
    let id = d.Util.id in
    let wordlst = Util.split_words d.Util.contents in
    let mkmap map word = M.add word [id] map in
      List.fold_left mkmap (M.empty) wordlst in
  let maps = S.map mapdoc docs in
  let combine key m1 m2 =
    match m1 with
      None -> m2
    | Some(x) -> match m2 with
        None -> m1
      | Some(y) -> Some(List.rev_append x y) in
  let finalmap = S.reduce (M.merge combine) (M.empty) maps in
  let bindings = M.bindings finalmap in
  let fixtype = function
      (key, lst) -> (key, List.rev_map string_of_int lst) in
    Util.print_reduce_results (List.rev_map fixtype bindings)
