(* Horace Chan (hhc39)
 * Ansha Yu (ay226)
 * PS5
 *)

open Common_nbody

(* Representation of a 2d rectangular plane *)
type bbox = { north_west:Plane.point; south_east:Plane.point }

(* Our representation of a Barnes-Hut tree. You are free to use this type
 * or create your own. *)
type bhtree = 
    Empty 
  | Single of body 
  | Cell of body * bbox * (bhtree Sequence.t)

(* [quarters box]: Divide a bounding box into 4 equally sized quadrants. *)
let quarters (box:bbox) : bbox Sequence.t = 
  let center = Plane.midpoint box.north_west box.south_east in
    match box.north_west , box.south_east, center with
      (nwx, nwy) , (sex, sey) , (cx, cy)->
        let mkbbox n = 
          (* The the first quadrant -> n=0. Counterclockwise *)
          if n = 1 then {north_west=box.north_west; south_east=center}
          else if n=3 then {north_west=center; south_east=box.south_east}
          else if n=2 then {north_west=(nwx, cy); south_east=(cx, sey)}
          else {north_west=(cx,nwy); south_east=(sex, cy)} in
          Sequence.tabulate mkbbox 4

(* [psuedobody bodies]: Return a psuedobody representing all bodies in [bodies]  *)
let pseudobody (bodies:body Sequence.t) : body = 
  let mkreduce (m1, r1, v1) (m2, r2, v2) =
    let new_mass = Plane.s_plus m1 m2 in
    let mr1 = Plane.scale_point m1 r1 in
    let mr2 = Plane.scale_point m2 r2 in
      if new_mass = 0. then (0.,(0.,0.),(0.,0.)) else
        let new_r = Plane.scale_point (1./.new_mass) (Plane.v_plus mr1 mr2) in
          (new_mass, new_r, (0.,0.)) in
    Sequence.reduce mkreduce (0.,(0.,0.),(0.,0.)) bodies

(* [make_bhtree bodies box]: Create Barnes-Hut tree of [bodies].
 * Raise an exception if a body in [bodies] is not contained within [box]. *)
let make_bhtree (bodies:body Sequence.t) (box:bbox) : bhtree =
  let rec bhtreehelp bod b =
    let qs = quarters b in
    let cx, cy = (Sequence.nth qs 1).south_east in
    let (nwx,nwy),(sex,sey) = b.north_west,b.south_east in
    let checkq0 x y = x>=cx && y> cy && x<=sex && y<=nwy || x=cx && y=cy in
    let checkq1 x y = x< cx && y>=cy && x>=nwx && y<=nwy in
    let checkq2 x y = x<=cx && y< cy && x>=nwx && y>=sey in
    let checkq3 x y = x> cx && y<=cy && x<=sex && y>=sey in
    let other = Sequence.filter (fun (_,(x,y),_) -> not (checkq0 x y) && 
            not (checkq1 x y) && not (checkq2 x y) && not (checkq3 x y)) bod in
      if Sequence.length other <> 0 then failwith "Body not contained in box"
      else if Sequence.length bod = 0 then Empty
      else if Sequence.length bod = 1 then Single(Sequence.nth bod 0)
      else 
        let q0 = Sequence.filter (fun (_,(x,y),_) -> checkq0 x y) bod in
        let q1 = Sequence.filter (fun (_,(x,y),_) -> checkq1 x y) bod in
        let q2 = Sequence.filter (fun (_,(x,y),_) -> checkq2 x y) bod in
        let q3 = Sequence.filter (fun (_,(x,y),_) -> checkq3 x y) bod in
        let mkqseq n = if n=0 then q0
                       else if n=1 then q1
                       else if n=2 then q2
                       else q3 in
        let qseq = Sequence.tabulate mkqseq 4 in
        let qzip = Sequence.zip (qs, qseq) in
        let rectree = Sequence.map (fun (q,subseq) -> bhtreehelp subseq q) in
          Cell(pseudobody bod, b, rectree qzip) in
    bhtreehelp bodies box

(* [in_range b1 b2]: returns true if the mass-weighted distance from
 * [b1] to [b2] is less than [theta]. Use to determine whether Barnes-Hut 
 * approximation is appropriate for a body and psuedobody. *)
let in_range (b1:body) (b2:body) (theta:Plane.scalar) : bool = 
  match b1, b2 with (m1, l1, v1),(m2, l2, v2) ->
    let d = Plane.distance l1 l2 in
      if d = 0. then true else
        theta >= (m2 /. d)

(* [acceleration tree theta body]: use [tree] and [theta] to approximate the
 * acceleration on [body] due to every other body in the tree.*)
let acceleration (tree:bhtree) (theta:Plane.scalar) (body:body) : Plane.vector =
  let naive_accel (b1:body) (b2:body) : Plane.vector =
	  match b1, b2 with (m1, l1, v1),(m2, l2, v2) ->
		  let d_unit_vec = Plane.unit_vector l1 l2 in
		  let d_scalar = Plane.distance l1 l2 in
		  if d_scalar = 0. then (0.,0.) else
	    let a_scalar = bigG *. m2 /. d_scalar /. d_scalar in
	    Plane.scale_point a_scalar d_unit_vec in

  let rec accel_help = function
      Empty -> (0.,0.)
    | Single(body2) -> naive_accel body body2
    | Cell(body2, box2, tree2) ->
      if in_range body body2 theta then naive_accel body body2
      else Plane.sum accel_help tree2 in
    accel_help tree
        
(* [accelerations bodies theta ox]: Approximate the acceleration vector for each 
 * body in [bodies]. *)
let accelerations (bodies:body Sequence.t) (theta:Plane.scalar) (box:bbox) 
    : Plane.vector Sequence.t = 
  let tree = make_bhtree bodies box in
  Sequence.map (fun b -> acceleration tree theta b) bodies

(* [update bodies theta box]: update the positions of all bodies in [bodies] via
 * Barnes-Hut approximation *)
let update (bodies:body Sequence.t) (theta:Plane.scalar) : body Sequence.t =
  let box : bbox = 
    let b = Sequence.nth bodies 0 in
    let get_coord f = fun (m1,(lx1,ly1),v1) (m2,(lx2,ly2),v2) ->
      let x : Plane.scalar ref = ref 0.0 in
      let y : Plane.scalar ref = ref 0.0 in
      if (f lx1 lx2) then x := lx1 else x := lx2;
      if (f ly1 ly2) then y := ly1 else y := ly2;
      (m1,(!x,!y),v1) in
      
    let (_,(x_max,y_max),_) = Sequence.reduce (get_coord (>)) b bodies in
    let (_,(x_min,y_min),_) = Sequence.reduce (get_coord (<)) b bodies in
    {north_west = (x_min,y_max); south_east = (x_max,y_min)} in
  
  let new_acc = accelerations bodies theta box in
  let combine = Sequence.zip (bodies, new_acc) in
  let update_with ((m1, l1, v1), a_new) = 
    let new_loc = 
      Plane.v_plus (Plane.v_plus l1 v1) (Plane.scale_point 0.5 a_new) in
    (m1, new_loc, Plane.v_plus v1 a_new) in
  Sequence.map update_with combine

(* [make_simulation bodies theta box]: create a stream representing the simulation. *)
let rec make_simulation (bodies:body Sequence.t) (theta:Plane.scalar) : simulation =
  Cons(bodies, fun () -> make_simulation (update bodies theta) theta)
