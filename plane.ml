(* Horace Chan (hhc39)
 * Ansha Yu (ay226)
 * PS5
 *)

type scalar = float
type point = scalar * scalar
type vector = point

let s_plus = ( +. )

let s_minus = ( -. )

let s_times = ( *. )

let s_divide x y = x /. y

let s_dist (a,b) (c,d) = sqrt ((c-.a)**2. +. (d-.b)**2.)

let s_compare a b = if (a>b) then 1 else if (a=b) then 0 else -1

let s_to_string s = string_of_float s

let v_plus (a,b)(c,d) = (a+.c, b+.d)

let distance (a,b) (c,d)= sqrt ((c-.a)**2. +. (d-.b)**2.)

let midpoint (a,b) (c,d) = ((a+.c)/.2., (b+.d)/.2.)

let head (a,b) = (abs_float a, abs_float b)

let sum f s = Sequence.map_reduce f (0.,0.) v_plus s

let scale_point s (x,y) = (s*.x, s*.y)

let unit_vector (a,b) (c,d) =
  let vec = (c-.a, d-.b) in
  let length = distance vec (0.,0.) in
    scale_point (1./.length) vec
