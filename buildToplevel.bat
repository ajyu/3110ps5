ocamlc -c -thread -I apps unix.cma threads.cma str.cma sequence.mli sequence.ml  apps/util.mli apps/util.ml apps/inverted_index.mli apps/inverted_index.ml apps/apm.mli apps/apm.ml apps/main_apps.ml
ocamlmktop -thread unix.cma threads.cma str.cma apps/util.cmo sequence.cmo apps/inverted_index.cmo apps/apm.cmo apps/main_apps.cmo -o apps/main_apps.exe
