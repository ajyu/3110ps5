(* Horace Chan (hhc39)
 * Ansha Yu (ay226)
 * PS5
 *)

type 'a t = 'a array

(* Karma: Multi_thread creates thread pools of 300 *)
let rec multi_thread (f: int -> unit) (n:int) =
  let k =300 in
  if n <= 0 then () else
  let a = Array.make (n - (max 0 (n-k))) (Thread.self ()) in
    for i=(max 0 (n-k)) to n-1 do
      a.(if n-k > 0 then i - (n-k) else i) <- Thread.create f i;
    done;
  Thread.join a.(0); Array.iter Thread.join a;
    multi_thread f (n-k)

let length = Array.length

let empty () = [||]

let cons (x:'a) (s:'a t) =
  let s_len = length s in
  let cons_seq : 'a t = Array.make (s_len+1) x in
  let func =(fun x -> cons_seq.(x+1) <- s.(x)) in
    multi_thread func s_len ; cons_seq

let singleton x = Array.make 1 x

let append s1 s2 = 
  if length s1 = 0 then s2 else if length s2 = 0 then s1 else
  let s1_len = length s1
  and s2_len = length s2 in
  let app_seq = Array.make (s1_len+s2_len) (s1.(0)) in
  let func1 = (fun x->app_seq.(x) <- s1.(x)) in
  let func2 = (fun x->app_seq.(x+s1_len) <- s2.(x)) in
    multi_thread func1 s1_len ;
    multi_thread func2 s2_len ;
    app_seq

let length = Array.length

let empty () = [||]

let cons (x:'a) (s:'a t) =
  let s_len = length s in
  let cons_seq : 'a t = Array.make (s_len+1) x in
  let func =(fun x -> cons_seq.(x+1) <- s.(x)) in
    multi_thread func s_len ; cons_seq

let singleton x = Array.make 1 x

let append s1 s2 = 
  if length s1 = 0 then s2 else if length s2 = 0 then s1 else
  let s1_len = length s1
  and s2_len = length s2 in
  let app_seq = Array.make (s1_len+s2_len) (s1.(0)) in
  let func1 = (fun x->app_seq.(x) <- s1.(x)) in
  let func2 = (fun x->app_seq.(x+s1_len) <- s2.(x)) in
    multi_thread func1 s1_len ; multi_thread func2 s2_len ;
    app_seq

let tabulate f n =
  if n < 0 then failwith "Choose an n > 0." else
  let a = Array.make n (f 0) in
  let tab i = a.(i) <- (f i) in
  (multi_thread tab n); a

let nth s n = 
  if (length s) = 0 then failwith "Sequence is empty." 
  else if (n < 0) || (n > ((length s)-1)) 
  then failwith "Specify n such that 0 < n < length(seq)"
  else Array.get s n

let map f s =
  if (length s) = 0 then (empty ()) else
  let map_seq = Array.make (length s) (f s.(0)) in
  let func = (fun x -> map_seq.(x) <- (f s.(x))) in
    multi_thread func (length s) ;
    map_seq

let reduce f b s =
  let mkreduce sequence newsequence i =
    let l = length sequence in
    if i = l-1 && l mod 2 = 1 then
      newsequence.(i/2) <- sequence.(i)
    else if i mod 2 = 0 then 
      newsequence.(i/2) <- f sequence.(i) sequence.(i+1) in
  let rec reducehelp seq =
    let len = length seq in
      if len = 0 then b
      else if len = 1 then f b seq.(0)
      else if len mod 2 = 0 then
        let newarr = Array.make (len/2) b in
          (multi_thread (mkreduce seq newarr) (len));
          reducehelp newarr
      else 
        let newarr = Array.make (len/2+1) b in
          (multi_thread (mkreduce seq newarr) (len));
          reducehelp newarr in
    reducehelp s

let filter f s =
  let f_arr = Array.make (length s) (empty ()) in
  let mkfilter i =
    f_arr.(i) <- if f s.(i) then (singleton s.(i)) else (empty ()) in
    multi_thread mkfilter (length s) ;
    reduce (append) (empty ()) f_arr

let map_reduce l e n s = reduce n e (map l s)

let repeat x n = Array.make n x

let flatten ss = reduce append (empty ()) ss

let zip (s1,s2) =
  if ((length s1) = 0 || (length s2) = 0) then empty () else
  let len = min (length s1) (length s2) in
  let zip_seq = Array.make len ((s1.(0), s2.(0))) in
  let zip_f = (fun x -> (zip_seq.(x) <- ((nth s1 x), (nth s2 x)))) in
    multi_thread zip_f len ;
    zip_seq

let split s i =
  if i > (length s) then failwith "Specify i such that 0 < i < length(seq)."
  else if i < 0 then failwith "Specify i such that 0 < i < length(seq)."
  else
	  let len = length s in
    let s1 = Array.make i s.(0) 
	  and s2 = Array.make (len-i) s.(0) in
	  let split_f = fun x ->
	    if x >= i then s2.(x-i) <- s.(x) else s1.(x) <- s.(x) in
	    multi_thread split_f len ;
	    (s1,s2)
