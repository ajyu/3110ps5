(* Horace Chan (hhc39)
 * Ansha Yu (ay226)
 * PS5
 *)

open Common_nbody 

(* [acceleration b1 b2] : compute gravitational acceleration vector for [b1] 
 * due to [b2]. *)
let acceleration (b1:body) (b2:body) : Plane.vector =
  match b1, b2 with (m1, l1, v1),(m2, l2, v2) ->
    let d_unit_vec = Plane.unit_vector l1 l2 in
    let d_scalar = Plane.distance l1 l2 in
      if d_scalar = 0. then (0.,0.)
      else let a_scalar = bigG *. m2 /. d_scalar /. d_scalar in
        Plane.scale_point a_scalar d_unit_vec

(* [accelerations bodies]: compute gravitational acceleration vector for
 *  each body in [bodies] *)
let accelerations (bodies:body Sequence.t) : Plane.vector Sequence.t = 
  let calc_acc b1 = Plane.sum (acceleration b1) bodies in
    Sequence.map calc_acc bodies

(* [update bodies]: apply acceleration to update the positions & velocities
 * of all bodies in [bodies] *)
let update (bodies:body Sequence.t) : body Sequence.t = 
  let new_acc = accelerations bodies in
  let combine = Sequence.zip (bodies, new_acc) in
  let update_with ((m1, l1, v1), a_new) =
    let new_loc = 
      Plane.v_plus (Plane.v_plus l1 v1) (Plane.scale_point 0.5 a_new) in
    (m1, new_loc, Plane.v_plus v1 a_new) in
    Sequence.map update_with combine

(* [make_simulation bodies]: Create a stream representation of the N-Body simulation  *)
let rec make_simulation (bodies:body Sequence.t) : simulation = 
  Cons(bodies , (fun () -> make_simulation (update bodies)))

