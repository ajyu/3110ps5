(* Horace Chan (hhc39)
 * Ansha Yu (ay226)
 * PS5
 *)

open Sequence
open Util

type profile = {
  firstname : string;
  lastname : string;
  sex : string;
  age : int;
  lo_agepref : int;
  hi_agepref : int;
  profession : string;
  has_children : bool;
  wants_children : bool;
  leisure : string;
  drinks : bool;
  smokes : bool;
  music : string;
  orientation : string;
  build : string;
  height : string
}

let convert (p : string) : profile =
  let s = String.concat " " (Str.split (Str.regexp_string "@") p) in
  Scanf.sscanf s "%s@ %s@ %s@ %d %d %d %s@ %B %B %s@ %B %B %s@ %s@ %s@ %s"
  (fun firstname lastname sex age lo_agepref hi_agepref profession has_children
       wants_children leisure drinks smokes music orientation build height ->
   { firstname = firstname;
     lastname = lastname;
     sex = sex;
     age = age;
     lo_agepref = lo_agepref;
     hi_agepref = hi_agepref;
     profession = profession;
     has_children = has_children;
     wants_children = wants_children;
     leisure = leisure;
     drinks = drinks;
     smokes = smokes;
     music = music;
     orientation = orientation;
     build = build;
     height = height
   })

let print_profile ({
     firstname = firstname;
     lastname = lastname;
     sex = sex;
     age = age;
     lo_agepref = lo_agepref;
     hi_agepref = hi_agepref;
     profession = profession;
     has_children = has_children;
     wants_children = wants_children;
     leisure = leisure;
     drinks = drinks;
     smokes = smokes;
     music = music;
     orientation = orientation;
     build = build;
     height = height } : profile) : unit =
  Printf.printf "%s %s\n" firstname lastname;
  Printf.printf "  sex: %s  age: %d  profession: %s\n" sex age profession;
  Printf.printf "  %s  %s\n" (if drinks then "social drinker" else "nondrinker") (if smokes then "smoker" else "nonsmoker");
  Printf.printf "  %s  %s\n"
    (if has_children then "has children" else "no children")
    (if wants_children then "wants children" else "does not want children");
  Printf.printf "  prefers a %s partner between the ages of %d and %d\n"
    (if (orientation="straight" && sex="F") || (orientation = "gay/lesbian" && sex="M") then "male" else "female")
    lo_agepref hi_agepref;
  Printf.printf "  likes %s music and %s\n" music leisure


let print_matches (n : string) ((p, ps) : profile * (float * profile) list) : unit =
  print_string "------------------------------\nClient: ";
  print_profile p;
  Printf.printf "\n%s best matches:\n" n;
  List.iter (fun (bci, profile) ->
    Printf.printf "------------------------------\nCompatibility index: %f\n" bci; print_profile profile) ps;
  print_endline ""


(* load various types of data from a file and convert to internal form
 * using the supplied 'convert' function *)
let load (filename : string) (convert : string -> 'a) : 'a list =
  try
    let f = open_in filename in
    let rec next accum =
      match (try Some (input_line f) with End_of_file -> None) with
      | None -> accum
      | Some line -> next (convert line :: accum) in
    let data = next [] in
    close_in f; data
  with exc -> failwith "malformed input"
  

(* This function takes several arguments as a string array: the app name, the *)
(* data file, the number of matches requested, and the first and last name of *)
(* the client, who should have a profile in the data file. The map step should*)
(* pair the client's profile with every other profile in the database and test*)
(* the match between them. For each pair, compute a compatibility index, which*)
(* is a float between 0. (perfectly incompatible) and 1. (perfectly compatible).*)
(* We leave the details of this computation up to you. Please include comments*)
(* as to your reasoning; be respectful and don't get too carried away. *)

(* The reduce step should use the compatibility index and the corresponding *)
(* pairs to create the desired number of matches. The results should then be *)
(* printed out the using the supplied print_matches function. The Util module*)
(* has some functions for reading in files and manipulating strings that may *)
(* be useful. *)


(* apm computes the potential love of your life.  The filename of a file
 * containing candidate profiles is in location 0 of the given array.
 * The number of matches desired is in location 1.  The first and last name
 * of the (human) client are in location 2 and 3, respectively.  The client's
 * profile must be in the profiles file.  Results are output to stdout. *)
let matchme (args : string array) : unit = 
  let profiles = load args.(0) convert in
  let num_matches_str = args.(1) in
  let client_name = args.(2) ^ args.(3) in
  let num_matches = int_of_string num_matches_str in
  
  if List.exists (fun x->((x.firstname)^(x.lastname)) = client_name) profiles 
  then 
    let client = 
      List.find (fun x->((x.firstname)^(x.lastname)) = client_name) profiles in
    
    let profiles = Array.of_list profiles in
    let profiles = tabulate (fun x -> profiles.(x)) (Array.length profiles) in
    
	  (* compatibility index: *)
	  let comp_idx client potential = 
	    let potential_name = (potential.firstname)^(potential.lastname) in
	    if client_name = potential_name then 0.0
	    else
        let score = ref 0.0 in
        (* age_pref *)
	      if (potential.age <= client.hi_agepref && 
	            potential.age >= client.lo_agepref) &&
	         (client.age <= potential.hi_agepref && 
	            client.age >= potential.lo_agepref) 
	      then score := !score +. 5.25;
        
        (* orientation *)
	      if (client.sex = "F" && client.orientation = "straight" && 
	            potential.sex = "M" && potential.orientation = "straight") ||
		         (client.sex = "M" && client.orientation = "straight" && 
		            potential.sex = "F" && potential.orientation = "straight") ||
	           (client.sex = "F" && client.orientation = "gay/lesbian" && 
	            potential.sex = "F" && potential.orientation = "gay/lesbian") ||
	           (client.sex = "M" && client.orientation = "gay/lesbian" && 
	            potential.sex = "M" && potential.orientation = "gay/lesbian")
	      then score := !score +. 5.25;
        
        (* wants_children *)
	      if (client.wants_children && potential.has_children) ||
	      (not client.wants_children && not potential.has_children)
	      then score := !score +. 3.8;
          
        (* miscellaneous *)
        if client.profession=potential.profession 
          then score := !score +. 1.1;
        if client.leisure = potential.leisure 
          then score := !score +. 1.6;
        if client.drinks = potential.drinks 
          then score := !score +. 1.4;
        if client.smokes = potential.smokes 
          then score := !score +. 1.9;
        if client.music = potential.music 
          then score := !score +. 1.2;
        
        (* total compatibility index score *)
        !score /. 21.5 in
	  
    (* sort sequence by compatibility *)
	  let matched_prof = map (fun x -> comp_idx client x) profiles in
    let unsorted_seq = zip (matched_prof, profiles) in
    
    (* helper function to transform sequence into an array *)
    let to_array s : 'a array =
      let len = length s in
      let arr = Array.make len (nth s 0) in
      for i = 0 to len-1 do
        arr.(i) <- (nth s i) 
      done; arr in
    
    (* sort the list of potentials based on rating *)
    let sort_f = fun (e1,_) (e2,_) ->
      if (e1 > e2) then (-1) else if (e1 < e2) then (1) else 0 in
    let sorted_seq : 'a array = (to_array unsorted_seq) in
    Array.sort sort_f sorted_seq;
    
    (* set the correct number of matches to display *)
    let corrected_num_matches = ref 0 in
    if num_matches > (Array.length sorted_seq)
    then corrected_num_matches := Array.length sorted_seq
    else corrected_num_matches := num_matches;
    let matched_lst =
      Array.to_list (Array.sub sorted_seq 0 !corrected_num_matches) in
    
	  print_matches num_matches_str (client, matched_lst)
  
  else print_string "Client does not exist in the database.\n"

