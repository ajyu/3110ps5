open Apm
open Inverted_index
let main =
  if Sys.argv.(1) = "mkindex" then mkindex Sys.argv.(2)
  else matchme (Array.of_list [Sys.argv.(2); Sys.argv.(3); Sys.argv.(4); Sys.argv.(5)])

(*make clean*)
(*make all  *)
(*ocamlmktop -thread unix.cma threads.cma str.cma ./apps/util.cmo sequence.cmo ./apps/inverted_index.cmo ./apps/apm.cmo ./apps/main_apps.cmo -o ./apps/main_apps.exe*)
(* cd apps *)
(* ./main_apps.exe matchme ./data/test_profiles_1.txt 5 Titus Hurst *)
(*./apps/main_apps.exe matchme ./apps/data/test_profiles_1.txt 5 Titus Hurst*)
(*./apps/main_apps.exe matchme ./apps/data/test_profiles.txt 5 Nolan Franco*)
